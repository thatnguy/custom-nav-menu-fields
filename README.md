# README #

A simple WordPress library used for creating custom fields for Nav Menus.

### Use this library for ###

* When you need to create text custom fields for nav menus as this is the only supported
  field type for now.
* When you only care about dealing with the custom field data yourself.

### How do I get set up? ###

* Place these files in your theme or you could event create a plugin yourself with these files.
* require these files in your theme/plugin
* register your fields using *custom_nav_menu_add_fields* hook.

### Example ###

```
#!php

add_filter( 'custom_nav_menu_add_fields', function( $fields ) {
        // register your fields here.
        // each field needs an identifier.
        // the field registered below will be identified as `menu-item-icon`
	$fields[ 'icon' ] = array( 'menu' => array(4), 'label' => 'Icon' );
	return $fields;
} );
```
### arguments for registering a field ###
* *label (string)* - label text which will be rendered for the fields label.
* *menu (array|string)* - menu(s) that this field will be assigned to. Inputs can be
                           menu id, menu slug or menu name.
* *class (string)* - extra classes to add to the field when rendering.