<?php

	// Custom Nav Menu Fields
	
	if ( ! class_exists( 'Custom_Nav_Menu_Fields' ) ) {

		class Custom_Nav_Menu_Fields {

			private $fields = array();

			private $path = '';

			public function __construct() {

				$this->path = dirname( __FILE__ );

				add_action( 'wp_loaded', array( $this, 'load' ), 10 );

				add_action( 'custom_nav_menu_get_fields', array( $this, 'get_fields' ), 10 );

				// use custom walker.
				add_filter( 'wp_edit_nav_menu_walker', array( $this, 'custom_nav_menu_walker' ), 99, 2 );

				// save field data.
				add_action( 'wp_update_nav_menu_item', array( $this, 'save_fields' ), 10, 3 );

			}

			public function load() {

				// retrieve user defined fields.

				$defaults = array(
					'type' => 'custom',
					'label' => '',
					// filter menu item by nav menu.
					// value can be menu ID, name, slug.
					// empty value will be ignored.
					'menu' => array(),

					// additional classes to add when rendering field.
					'class' => '',
				);

				foreach ( apply_filters( 'custom_nav_menu_add_fields', $this->fields ) as $key => $settings ) {

					// need to have a proper field identifier.
					if ( is_int( $key ) ) {
						continue;
					}

					if ( ! is_array( $settings ) ) {
						$settings = array( 'label' => $settings );
					}

					if ( ! isset( $settings['label'] ) || ( isset( $settings['label'] ) && empty( $settings['label'] ) ) ) {
						// use the identifier as label.
						$settings['label'] = ucfirst( $key );
					}


					$settings = wp_parse_args( $settings, $defaults );

					if ( ! is_array( $settings['menu'] ) ) {
						$settings['menu'] = array( $settings['menu'] );
					}

					// filter out bad menu identifier.
					// e.g. if menu slug 'hello-world' is passed and is not found then ignore this menu.
					$_menus = array();
					foreach ( $settings['menu'] as $i => $id ) {

						if ( ! is_numeric( $id ) ) {
							$menu_obj = wp_get_nav_menu_object( $id );

							if ( ! $menu_obj ) {
								continue;
							}

							$id = $menu_obj->term_id;
						}

						$_menus[] = (int) $id;
					}

					$settings['menu'] = $_menus;
					unset( $_menus );

					$this->fields[ $key ] = $settings;
				}

				// do init stuff here.
			}

			public function get_fields( $item ) {
				return $this->fields;
			}

			public function save_fields( $menu_id, $menu_item_db_id, $menu_item_args ) {

				if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
					return;
				}

				// nonce check.
				check_admin_referer( 'update-nav_menu', 'update-nav-menu-nonce' );

				foreach ( $this->fields as $key => $settings ) {
					$k = "menu-item-{$key}";

					if ( ! isset( $_POST[ $k ][ $menu_item_db_id ] ) ) {
						continue;
					}

					$v = $_POST[ $k ][ $menu_item_db_id ];

					if ( ! empty( $v ) ) {
						update_post_meta( $menu_item_db_id, $k, $v );
					} else {
						delete_post_meta( $menu_item_db_id, $k );
					}
				}

			}

			// @todo
			public function custom_nav_menu_register_fields( $menu_item ) {
				return $menu_item;
			}

			public function custom_nav_menu_walker( $walker, $menu_id ) {

				require_once $this->path . '/custom-nav-menu-fields-walker.class.php';

				$walker = 'Custom_Nav_Menu_Fields_Walker';

				return $walker;
			}
		}
	}

	new Custom_Nav_Menu_Fields();
